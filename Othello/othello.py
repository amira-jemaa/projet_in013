#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import reduce

import sys
sys.path.append("..")
import game
# plateau: List[List[nat]]
# liste de listes (lignes du plateau) d'entiers correspondant aux contenus des cases du plateau de jeu

# coup:[nat nat]
# Numero de ligne et numero de colonne de la case correspondante a un coup d'un joueur

# Jeu
# jeu:[plateau nat List[coup] List[coup] List[nat nat]]
# Structure de jeu comportant :
#           - le plateau de jeu 0
#           - Le joueur a qui c'est le tour de jouer (1 ou 2) 1
#           - La liste des coups possibles pour le joueur a qui c'est le tour de jouer 2
#           - La liste des coups joues jusqu'a present dans le jeu 3
#           - Une paire de scores correspondant au score du joueur 1 et du score du joueur 2 4
def initialiseJeu():
    """ void -> jeu
        Initialise le jeu (nouveau plateau, liste des coups joues vide, liste des coups valides None, scores a 0 et joueur = 1)
         
    """
    
    plateau = [[0 for i in range(8)] for i in range(8)]

    plateau[3][3] = 1
    plateau[4][4] = 1
    plateau[4][3] = 2
    plateau[3][4] = 2
    
   
    

    return [plateau , 1, None, [], [2,2]]


def coups(jeu):
    """
    The function coups returns the list of the set of empty squares around the opponent's pieces
    
    :param jeu: the current state of the game
    :return: A list of all the set of empty squares around the opponent's pieces.
    """
     
    adv = jeu[1]%2 + 1
    s = [entourageVide(jeu, l, c) for l in range(8) for c in range(8) if jeu[0][l][c]==adv]
    #reduce pour ne pas avoir des redondances des mêmes directions
    s = reduce(lambda a,b:a|b,s)
    return s

def entourageVide(jeu, l, c):
    """
    It returns the set of all the cases around the case (l,c) of the openent that are empty
    
    :param jeu: the game state
    :param l: the line of the cell
    :param c: column
    :return: A set of tuples (l,c) where l and c are the coordinates of the eachsquare
    """
   
    return {(l+i, c+j) for i in [-1,0,1] for j in [-1,0,1] if (c+j <= 7) and (c+j>=0) \
    and (l+i<=7) and (l+i>=0) and jeu[0][l+i][c+j] == 0}

def getCoupsValides(jeu):
    """
    The function getCoupsValides(jeu) returns the list of all the valid moves for the current player
    
    :param jeu: the game in its current state
    :return: A list of all possible moves
    """
     
    if(jeu[2]==None):
        coup = coups(jeu)
        jeu[2] = [x for x in coup if len(getEncadrements(jeu, x , False)) > 0]
    return jeu[2]

def getEncadrements(jeu, c, all = True):
    """
    Returns a list of directions in which it is possible to sandwich a piece of the opponent
     
    """
  
    ret = []
    for i in [-1, 0, 1]:
        for j in [-1, 0, 1]:
            if(i==0 and j ==0):
                continue
            #check si il est possible d'encadré une pièce de l'adversaire
            if checkEncadrementDirection(jeu, c, i, j):
                ret.append((i,j))
                if not all:
                    break
    return ret

def checkEncadrementDirection(jeu, cp , i ,j):
    """
    The function checkEncadrementDirection(jeu, cp , i ,j) checks if the piece cp can be encadred in the
    direction (i,j)
    :return: The boolean value of the attribute atr.
    """
    
    atr = False
    l, c = cp
    while True:
        l+=i
        c+=j
        if(l>7 or l<0 or c>7 or c<0):
            return False
        if(jeu[0][l][c] == 0):
            return False
        if(jeu[0][l][c] == jeu[1]):
            return atr
        atr = True

def joueCoup(jeu, cp):
    """
    Given a game state and a move, play the move on the board 
    """
    #place le coup sur le board
    jeu[0][cp[0]][cp[1]] = jeu[1]
    #modifie un point au tuple soit x soit y selon le joueur
    jeu[4][game.getJoueur(jeu)-1] += 1
    d = getEncadrements(jeu, cp)
    for x in d:
        returnPions(jeu, cp, x)
    #ajoute ce coup aux coups jouée 
    jeu[3].append(cp)
    # efface la liste des coups valides et la remet à None
    jeu[2] = None
    #change de joueur
    jeu[1] = jeu[1]%2 +1



def returnPions(jeu, cp, x):
    """ jeu->void
        retourne les pions dans la direction x
    """
    joueur = game.getJoueur(jeu)
   

  
    while(jeu[0][cp[0]+x[0]][cp[1]+x[1]] == joueur % 2 + 1):
        jeu[0][cp[0]+x[0]][cp[1]+x[1]] = joueur

        jeu[4][joueur-1]+=1
        jeu[4][joueur%2]-=1

        cp = cp[0]+x[0],cp[1]+x[1]
  
        
def finJeu(jeu):
    """
    The function finJeu(jeu) returns True if the game is over and False otherwise
    
    :param jeu: the current state of the game
    :return: a bool value
    """
    
    if(jeu[4][0] == 0 or jeu[4][1] == 0):
        return True
    if(jeu[4][0]+jeu[4][1] == 64):
        return True

    if(game.getCoupsValides(jeu) == []):
        return True
    return False

def affiche(jeu):
    """ jeu->void
        Affiche l'etat du jeu de la maniere suivante :
                 Coup joue = <dernier coup>
                 Scores = <score 1>, <score 2>
                 Plateau :

                         |       0     |     1       |      2     |      ...
                    ------------------------------------------------
                      0  | <Case 0,0>  | <Case 0,1>  | <Case 0,2> |      ...
                    ------------------------------------------------
                      1  | <Case 1,0>  | <Case 1,1>  | <Case 1,2> |      ...
                    ------------------------------------------------
                    ...       ...          ...            ...
                 Joueur <joueur>, a vous de jouer
                    
         Hypothese : le contenu de chaque case ne depasse pas 5 caracteres
    """

    if(jeu[3]== []):
        print("Coup joue = None")
    else:
        print("Coup joue = ", jeu[3][-1])
    print("Joueur: ", jeu[1])
    print("Scores = ", jeu[4][0]," , ", jeu[4][1])
    print("Plateau :")
    
    plateau = jeu[0]
    
    for x in range (len(plateau[0])):
        if(x == 0):
            print("%5s|" %(""), end=" ")
        print("%5s|" %(x), end=" ")
    print()
    print("--------------------------------------------------------------")

    for i in range(len(plateau)):
        print(" ",i," |", end=" ")
        for j in range(len(plateau[i])):
            if(plateau[i][j] == 0):
                print("%5s|" %(" "), end=" ")
            else:
                if(plateau[i][j] == 1):
                    print("\x1b[0;30;43m%5s\x1b[0m|" %(plateau[i][j]), end=" ")
                else:
                    print("\x1b[0;30;41m%5s\x1b[0m|" %(plateau[i][j]), end=" ")
        print()
        print("--------------------------------------------------------------")
