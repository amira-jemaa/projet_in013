#!/usr/bin/env python
# -*- coding: utf-8 -*-
import othello
import sys
sys.path.append("..")
import game
game.game=othello
sys.path.append("./Joueurs")
import joueur_random
import joueur_humain
import joueur_premier_coup
import time
PARTIES=300
list_joueurs = [joueur_random, joueur_premier_coup]
game.joueur1 = joueur_random
game.joueur2 = joueur_premier_coup

def printJoueur(joueur):
    if(joueur == joueur_random):
        return "joueur_random"
    if(joueur == joueur_premier_coup):
        return "joueur_premier_coup"

for joueur2 in list_joueurs:
    game.joueur2 = joueur2
    J1 = printJoueur(game.joueur1)
    J2 = printJoueur(game.joueur2)
    j1_res = 0
    j2_res = 0

    print(J1," vs ", J2)
    for i in range(PARTIES):
        jeu = game.game.initialiseJeu()
        it = 0   
        while(it < 100 and not game.finJeu(jeu)):
            if(jeu[1] == 1):
                coup = game.joueur1.saisieCoup(jeu)
            else:
                coup = game.joueur2.saisieCoup(jeu)
            game.joueCoup(jeu, coup)
            it=it+1
        if(game.getGagnant(jeu) == 1):
            j1_res=j1_res+1
        else:
            j2_res=j2_res+1
        print('#%s:%s scored %s points. %s scored %s points.' % (i + 1,
          J1,jeu[4][0], J2, jeu[4][1]))

print(J1+' gagne %s (%s%%)!' % (j1_res, round(j1_res / PARTIES*100,1)))
print(J2+' gagne %s (%s%%)!' % (j2_res, round(j2_res / PARTIES*100,1)))

