- [Introduction à Othello:](#introduction-à-othello)
- [Régles d'Othello:](#régles-dothello)
- [Introduction et Régles d'Awele:](#introduction-et-régles-dawele)
- [Les agents / Joueurs :](#les-agents--joueurs-)
- [Tests Agents:](#tests-agents)
- [Joueur Horizon:](#joueur-horizon)
- [Joueur Aleatoire:](#joueur-aleatoire)
- [Joueur Premier Coup:](#joueur-premier-coup)
- [Joueur Humain:](#joueur-humain)
- [Algorithme de Recherche MinMax:](#algorithme-de-recherche-minmax)

L'approche consistait à étudier de nombreuses techniques courantes d'intelligence artificielle dans les jeux de société et à voir comment chacune était utilisée. Il était également important de voir certaines solutions créatives spécifiques à un jeu afin de s'en inspirer pour développer des techniques personnalisées. Après avoir fait cela comme pratique, un agent a été créé pour un jeu très simple. Des heuristiques simples ont été utilisées avec un mélange d'approches offensives et défensives.




# Introduction à Othello:
 Othello, aussi connu sous le nom de Réversi, est un jeu de stratégie
 à deux joueurs. Il se joue sur un plateau de 64 cases appelé othellier. 
 Le jeu disposent de 64 pions bicolores, noirs d'un côté et blancs de l'autre. Un pion est dit noir (respectivement blanc) si sa face visible est celle de couleur noire (respectivement blanc).
# Régles d'Othello:
Le but du jeu est d'avoir plus de pions que son adversaire à la fin de la partie. Celle-ci se termine lorsque aucun des deux joueurs ne peut plus jouer de coup qui soit légal.


Le joueur qui a le trait doit poser un pion de sa couleur sur une case vide de l'othellier qui soit adjacente à un pion adverse. Pour que le coup soit légal, le pion posé doit encadrer un ou plusieurs pions adverses avec un autre pion allié déjà placé sur l'othellier. Pour chaque direction, il retourne les pions qu'il vient d'encadrer du côté de ça couleur. 
 
# Introduction et Régles d'Awele:
Les règles de ce jeu sont particulièrement simples et s'apprennent en quelques minutes. Le
jeu se joue à deux, à tour de rôle, l'un des joueurs pouvant être un ordinateur. Il y a
initialement quatre graines dans chacune des cases du plateau, soit 48 graines.
A tour de rôle, chaque joueur saisit les graines de l'une de ses six cases – celles du plateau se
trouvant devant lui – et les sème une à une dans les cases suivantes dans le sens inverse des
 aiguilles d'une montre.
 Après cette distribution si la dernière graine semée tombe dans un trou de l'adversaire comportant déjà une ou deux graines, le joueur récolte les deux ou trois graines de ce trou et les place dans son grenier. Le joueur récupère également les graines des trous précédents de la rangée de l’adversaire qui respectent cette condition, mais ne peut pas écolter le contenu de plus de quatre trous consécutifs. Une règle d'or est que la prise de ces graines est obligatoire. Si avec le nombre de graines présentes dans l'une de ses cases le joueur peut faire un ou plusieurs tours du plateau, alors il devra sauter systématiquement la
case de départ. 
Un joueur ne peut pas affamer l'adversaire délibérément sauf s'il n'a pas d'autre coup à jouer :
ceci signifie que, dans l'esprit de l'awalé il est impératif de jouer un coup qui permette à
l'adversaire de rejouer ensuite. Si ce n'est pas possible, la partie s'arrête. Celui qui devait jouer récupère les graines restantes et le joueur adverse reçoit une graine qu'il ajoute à sa propre récolte. 
La partie s'arrête dès qu'un joueur a gagné au moins 25 graines.  La partie peut être 
 nulle aussi si le jeu se trouve dans un processus récurrent ou s'il ne reste plus que six graines ou moins. Si la même configuration réapparaît de façon périodique ou s'il n'est plus possible de jouer, la partie est arrêtée et le joueur ayant le plus de graines est déclaré vainqueur.
# Les agents / Joueurs : 
Le joueur est une structure très simple. Comme dans la vie de tout les jours on attribut un nom à chaque personne. C'est ce qui va être fait pour cette structure. En outre, en petit plus doit intervenir du faire de l'intelligence artificielle : Le type. Le type d'un joueur est une énumération des niveaux de difficultés.
# Tests Agents: 
Plusieurs expériences ont été menées pour évaluer les performances globales de chaque agent et pour déterminer dans quelles conditions l'agent excellait ou non. Ces expériences ont consisté en plusieurs jeux de test joués contre quatre agents ainsi que contre des joueurs humains ayant des niveaux de compétence différents, imposés par eux-mêmes.

# Joueur Horizon:

`Ce joueur adopte une approche gourmande pour choisir un coup grace à la fonction d'évaluation qui retourne son meilleur coups. Il parcourt sa liste de coups valides et choisit celui qui renverse le plus de pièces adverses. Or, ceci est une mauvaise approche dans le jeu d'Othello,car le nombre de pièces que l'on possède au début et au milieu de la partie est souvent une indication incorrecte de la performance réelle de son coup valide. Par conséquent, un joueur qui prend ses décisions uniquement en fonction du nombre de pièces qu'il peut retourner ne sera pas performant.`

# Joueur Aleatoire:

`L'agent aléatoire est l'agent le plus simple car il choisit simplement un coup au hasard dans la liste des coups valides.`

# Joueur Premier Coup:

Ce joueur joue toujours avec le premier coup valide dans la liste des coups valides

# Joueur Humain:
`Enfin, des joueurs humains ont testé leurs compétences contre l'agent. Comme les humains sont les plus imprévisibles et non déterministes, cela a servi de véritable test des capacités de l'agent. Trois joueurs, dont le niveau de compétence était de un, cinq et sept sur dix (dix étant un joueur expert et un étant un nouveau venu), ont joué respectivement deux, cinq et deux parties. La moyenne des scores de l'agent et des joueurs était calculée pour chaque manche avec la même configuration d'agent et le gagnant était celui qui avait le score le plus élevé. Chaque joueur humain a joué en tant que joueur blanc et noir contre l'agent avec des configurations de Minimax uniquement aux profondeurs un et six, avec et sans min attendu ; et avec tous les exploits actifs aux profondeurs un et six de Minimax, avec et sans min attendu.`

# Algorithme de Recherche MinMax:

Comme la plus part des jeus de réflexion Othello est un jeu dits
'jeu à deux joueurs, à somme nulle et information complète'. A somme nulle
car Les gains d’un joueur sont exactement l’opposé des gains de l’autre
joueur (ce qui me fait gagner fait perdre mon adversaire). A information
complète car lors du choix d’un coup à jouer chaque joueur connaît
précisément ses possibilités d’action et celle de son opposant. De plus il
connait les gains résultants de ces actions.
L'algorithme utilisé pour cette configuration est l'algorithme minmax auquel certaines améliorations pourront être apportées. Min-max est un
algorithme qui reproduit la manière naturelle qu'un humain mettrai en
œuvre pour jouer. Il consiste donc à anticiper la réplique de l'adversaire
à un coup joué, puis la réplique de cette réplique … ainsi de suite
jusqu'à une certaine condition d'arrêt. Une fois cette condition vérifiée,
l'algorithme utilisera une fonction d'évaluation qui permettra de rendre
compte de la situation dans laquelle se trouve un joueur étant donné
l'othellier. Min-max permet de retourner l'évaluation correspondant au
meilleur compromis maximisation-minimisation pouvant être fait à partir
d'une situation. Soit la meilleur évaluation en supposant que l'adversaire
ai une évaluation équivalente du jeu.
L'arbre généré par la récursion de min-max aura autant de branches
que de case jouables sur le damier et ce pour tous les nœuds de cet
arbre : La complexité de min-max est exponentiel. En effet, puisqu'en
moyenne un joueur d'Othello à 8 choix de coup, alors, pour un arbre de
hauteur h on observera 8^h fonction d'évaluation calculées. La complexité
de l'algorithme nous contraint donc (la plus part du temps) à ne pas
attendre la fin de partie pour restreindre la profondeur, mais plutôt à
fixer une profondeur avant de commencer la recherche.

 

